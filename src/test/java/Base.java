
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.BeforeMethod;


public class Base {
    WebDriver driver;
    EventFiringWebDriver eventDriver;

    @BeforeMethod
    public void before() {
        driver = BrowserFactory.defineBrowser("chrome");
        eventDriver = new EventFiringWebDriver(driver);
        eventDriver.register(new EventHandler());
        System.out.println("this is working");
    }
}
