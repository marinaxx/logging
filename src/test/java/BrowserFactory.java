import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BrowserFactory {

    public static WebDriver defineBrowser(String name) {
        WebDriver driver = null;
        if (name.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "/Users/marina/Hillel/Logging/src/main/resources/chromedriver");
            return new ChromeDriver();
        }
        if (name.equalsIgnoreCase("firefox")) {
            System.setProperty("webdriver.gecko.driver", "/main/resources/geckodriver");
            return new FirefoxDriver();
        }
        else
            return null;
    }
}
